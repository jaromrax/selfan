from selfan.version import __version__
from fire import Fire

from selfan import config
import os
import datetime as dt

from flask import Flask, render_template, render_template_string, Response, url_for, jsonify
from flask import request
from flask import jsonify, send_file

from selfan.mmapwr import mmwrite
import json

from werkzeug.utils import secure_filename



remote_ip = "x"
#======== DEFINE THE CONFIG FILE HERE ========

config.CONFIG['confdir'] = "~/.config/selfan/"
if config.CONFIG['confdir'][-1]!="/": config.CONFIG['confdir']+="/"

config.CONFIG['filename'] = config.CONFIG['confdir']+"cfg.json"
config.CONFIG['mycode'] =   config.CONFIG['confdir']+"mycode.py"
config.CONFIG['port'] = 8999

for i in ['confdir','filename','mycode']:
    config.CONFIG[i] = os.path.expanduser( config.CONFIG[i] )

#================= config  ===============

res = config.load_config()
if res:
    print(f"i... load_config ...  result={res}")
    print(f"{config.CONFIG}")
else:
    print(f"X... load_config result:{res}")

if not res:
    print("i... first config write/save")
    config.save_config()

#======================= config loaded or saved ==========





app = Flask(__name__)
#app.config["UPLOAD_FOLDER"] = "static/"
app.config['SECRET_KEY'] = 'kljhadfilhuli4788g'



index_page = f"""
 <!--meta http-equiv="refresh" content="5";-->

<html>
 <head>
<title>{dt.datetime.now()}</title>
</head>
<body>
Hello from selfan.web
</body>
</html>
"""

#=======================================================================================functions




def track_demand():
    #print(f"D... WEB: {dt.datetime.now()}")
    if not config.load_config():
        print("X... problem loading config... trackdemand")

    dic={}
    remote_ip=request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    #print(f"i... index access IP = {request.remote_addr},{remote_ip}")
    #dic['t0'] = dt.datetime.now() # impossible to jsonify
    dic['t0'] = dt.datetime.now().strftime("%Y%m%d_%H%M%S")
    dic['ipx'] = remote_ip
    dic['ip'] = request.remote_addr
    dic['ver'] = __version__
    if 'peers' in config.CONFIG.keys():
        dic['peers'] = config.CONFIG['peers']
    else:
        dic['peers'] = []
    if 'myip' in config.CONFIG.keys():
        dic['myip'] = config.CONFIG['myip']
    else:
        dic['myip'] = None

    return dic



@app.route('/getcode', methods = ['GET','POST'] )
def getcode():
    dic =track_demand()
    print(f"i... {dic['ip']} WANTS THE CODE !!! ")
    #print( json.dumps(dic, indent=4, sort_keys=True))
    if os.path.exists(config.CONFIG["mycode"]):
        try:
            print("D... sending code", config.CONFIG["mycode"])
            return send_file(config.CONFIG["mycode"] , as_attachment=True)
        except Exception as e:
            print("X... cannot send the code, exception")
    else:
        print("X... no code sent,", config.CONFIG["mycode"],"does not exist")

    return jsonify(dic)


@app.route('/getcodesign', methods = ['GET','POST'] )
def getcodesign():
    dic =track_demand()
    print(f"i... {dic['ip']} WANTS THE CODE !!! ")
    #print( json.dumps(dic, indent=4, sort_keys=True))
    if os.path.exists(config.CONFIG["mycode"]+".asc"):
        try:
            print("D... sending code", config.CONFIG["mycode"]+".asc")
            return send_file(config.CONFIG["mycode"]+".asc" , as_attachment=True)
        except Exception as e:
            print("X... cannot send the code, exception")
    else:
        print("X... no code sent,", config.CONFIG["mycode"]+".asc","does not exist")

    return jsonify(dic)




@app.route('/uploader', methods = [ 'POST', 'PUT'])
def uploader():
    #if request.method == 'POST':
    print("i... UPLOAD ONLY LOCALY!, agents need to decide and retrieve from main...")
    print("D... POSTING seen....", request.files)

    file1 = request.files['filedata']   # gives you a FileStorage
    #test = request.form['name']        # gives you the string 'Test'

    #print(file1, test )

    f = request.files['filedata']
    print(f)
    filename = secure_filename(f.filename)
    print("i... filename= ",filename)
    if not os.path.exists(config.CONFIG["confdir"] + f"suggested"):
        os.mkdir(config.CONFIG["confdir"] + f"suggested")
    dest = config.CONFIG["confdir"] + f"suggested/{filename}"
    f.save( dest)
    print(f"i... file saved to {dest}")
    return 'file uploaded successfully'





@app.route('/')
def report():
    dic =track_demand()
    print("D... web demand: only report")
    #print( json.dumps(dic, indent=4, sort_keys=True))
    return jsonify(dic)





@app.route('/cmd/<expr>')# , endpoint="cmd")
@app.route('/cmd/<expr>/<value>')# , endpoint="cmd")     #dic['cmd'] = request.endpoint
def index(expr,value=""):
    global remote_ip

    #print("x... ",value)
    dic =track_demand()

    if value=="":
        dic['cmd']=expr
    else:
        dic['expr']=expr
        dic['value']=value

    res = json.dumps( dic ) # all coordinates + command
    mmwrite( res )
    #mmwrite( dic['cmd'] )
    return jsonify(dic)
    return render_template_string(index_page)
    return "ahoj"# render_template_string(index_page)
