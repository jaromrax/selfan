#!/usr/bin/env python3

# to override print <= can be a big problem with exceptions
#from __future__ import print_function # must be 1st
import builtins
import time

print("\n\n","_"*78,"MAIN")
#print("D... sleeping before starting ")
#time.sleep(3)

import sys

from fire import Fire

from selfan.version import __version__
from selfan import unitname
from selfan import config


import os
import sys
import time
import datetime as dt


import netifaces as ni



from selfan import web # THIS IS THE MOST IMPORTANT PART:CONFIG LOAD

import multiprocessing
from selfan.mmapwr import mmread_n_clear, mmcreate
import sys
import json

import subprocess as sp
import gnupg
import shutil

import re

import requests


class Bcolors:
    HEADER = '[95m'
    OKBLUE = '[94m'
    OKGREEN = '[92m'
    WARNING = '[93m'
    FAIL = '[91m'
    ENDC = '[0m'
    BOLD = '[1m'
    UNDERLINE = '[4m'

#================================================================= functions

def versiontuple(v):
    if v.find("-dev")>0:
        v = re.sub( "-dev","", v )
    tup =  tuple(map(int, (v.split("."))))
    #print(tup)
    return tup


def get_version( SUGPATH):
    with open( SUGPATH,"r") as f:
        res = f.readlines()
        res = "\n".join(res)
    m = re.search(r'__version__="(.+)"', res)
    print( f"i... /{m.group(1)}/ IN {SUGPATH}")
    #(my version=={__version__})" )
    return m.group(1)


def gpg_verify_mycode( SUGPATH ):
    gpg = gnupg.GPG(gnupghome= os.path.expanduser('~/.gnupg/') )
    print("D... ************ gpg ************************")
    print(f"D... testing {SUGPATH}")

    stream = open( SUGPATH+".asc", "rb" )

    verified = gpg.verify_file( stream, SUGPATH )
    print("D... MY TRUST=", verified.trust_level,"   (should be >=", verified.TRUST_FULLY,")")
    trusted = False
    if verified.trust_level is not None and verified.trust_level >= verified.TRUST_FULLY:
        print('D... Trust level: %s' % verified.trust_text)
        trusted = True
    else:
        print(f"X... Not trusted {SUGPATH}")
    stream.close()
    print("D... ************ gpg ************************")
    return trusted




def gpg_copy_mycode():

    BASENAME = os.path.basename(   config.CONFIG['mycode']  )
    SUGPATH =  config.CONFIG['confdir'] + f"suggested/{BASENAME}"
    if os.path.exists(SUGPATH):
        vn = get_version( SUGPATH )
        if versiontuple(vn) > versiontuple(__version__):
            print(f"i... NEW VERSION !!!   /{vn}/ > /{__version__}/ ")
            if gpg_verify_mycode(SUGPATH):
                print(f"i... copying a new code from {SUGPATH}")
                shutil.copyfile( SUGPATH,            config.CONFIG['mycode']   )
                shutil.copyfile( SUGPATH+".asc",     config.CONFIG['mycode']+".asc" )
                return True
        else:
            print("D.. OLD OR SAME VERSION ... not copying anything")

    return False





def process_commands( t ):
    dt = 0.1
    i = int(t/dt)
    while i>0: #=============================== LOOP============

        if config.CONFIG['myip'] in config.CONFIG['peers']:
            config.CONFIG['peers'].remove(config.CONFIG['myip'])

        print(f">{i:05d}:", end="\r")
        cmd,expr,value = "","",""

        res = mmread_n_clear()
        #print("DIRCT",res)
        try:
            res = json.loads( res )
        except:
            res = {}

        if res != {}: # pretty print
            print( json.dumps(res, indent=4, sort_keys=True))



        #   --------------------------------------------- CONNAMDS

        if 'cmd' in res: cmd = res['cmd'].strip()
        if 'expr' in res: expr = res['expr'].strip()
        if 'value' in res: value = res['value'].strip()

        #================================================

        if len(expr)>0 and len(value)>0:
            print(f"i... {expr} := {value}")

            if expr == "addpeer":
                if not 'peers' in config.CONFIG.keys():
                    config.CONFIG['peers'] = []
                config.CONFIG['peers'].append( value )
                config.CONFIG['peers'] = list(set( config.CONFIG['peers'] ) )
                print( json.dumps( config.CONFIG, indent=4, sort_keys=True))
                config.save_config()

            if expr == "delpeer":
                if not 'peers' in config.CONFIG.keys():
                    config.CONFIG['peers'] = []
                else:
                    if value in config.CONFIG['peers']:
                        config.CONFIG['peers'].remove( value )
                        print( json.dumps( config.CONFIG, indent=4, sort_keys=True))
                        config.save_config()


        if cmd=="quit":
            print("X... quit detected")
            return True

        if cmd=="reload":
            print("X... reload detected")
            return False

        if cmd=="delconf":
            print("X... delconf detected")
            print(f"X... deleting {config.CONFIG['filename']} is 3 seconds")
            time.sleep(3)
            os.remove( config.CONFIG['filename']  )
            return False



        # ------------------------------------------------ DUTIES ----

        # if i%30==0:
        #     print("D... ping peers")
        #     for j in config.CONFIG['peers']:
        #         print(f"D... ping {j}")


        if i%39==0:
            #print("D... get report from peers")
            for j in config.CONFIG['peers']:
                # REPORT
                webaddr = f'http://{j}:8999/'
                print(f"D...  REPORT FROM {webaddr:28s}", end=" ")
                ok = False
                try:
                    res = requests.get( webaddr  , verify=False, timeout=1)
                    res = res.json()
                    print( "i... ok" )
                    ok = True
                except:
                    print("X... no response")

                if ok:

                    # 1 SCAN INSIDE my[] PEERS, ADD peers to target
                    for k in sorted(config.CONFIG['peers']):
                        if not (k in res['peers']) and  (k!=j): # not itself
                            webaddr = f'http://{j}:8999/cmd/addpeer/{k}'
                            res2 = requests.get( webaddr  , verify=False, timeout=1)
                    myip = config.CONFIG['myip']
                    if not (myip in res['peers']): # not me
                            webaddr = f'http://{j}:8999/cmd/addpeer/{myip}'
                            res2 = requests.get( webaddr  , verify=False, timeout=1)

                    # 2 IF CHECK VERSION
                    vertar = res['ver']
                    if versiontuple(vertar) > versiontuple(__version__):
                        print("X... REMOTE VERSION BETTER",vertar)

                        webaddr = f'http://{j}:8999/getcode'
                        res3 = requests.get( webaddr  , verify=False, timeout=1)

                        if not os.path.exists(config.CONFIG["confdir"] + f"suggested"):
                            os.mkdir(config.CONFIG["confdir"] + f"suggested")
                        dest = config.CONFIG["confdir"] + f"suggested/mycode.py"
                        with open( dest, "w" ) as f:
                            f.write(str(res3.content))

                        webaddr = f'http://{j}:8999/getcodesign'
                        res3 = requests.get( webaddr  , verify=False, timeout=1)

                        dest = config.CONFIG["confdir"] + f"suggested/mycode.py.asc"
                        with open( dest, "w" ) as f:
                            f.write(str(res3.content))
                        return False

        time.sleep( dt )
        i-=1
    #===================================LOOP ===========================
    return False








def get_myip( interface = 'tailscale0'):

    myip = None
    try:
        myip = ni.ifaddresses( interface )[ni.AF_INET][0]['addr']
        print(f"i... interface  ... {myip}")
    except:
        print(f"X... no ip known on /{interface}/ interface")
    return myip






def respawn( t = 3, process = None):
    args = sys.argv[:]


    #print(f"D... checking {config.CONFIG['mycode']}   in {config.CONFIG['confdir']}")

    if gpg_copy_mycode(): # try to copy after
        print("i... new TRUSTED code copied")

    if os.path.exists( config.CONFIG["mycode"] ):
        #print(f"i... MYCODE exists in  {config.CONFIG['mycode']}")
        if  gpg_verify_mycode( config.CONFIG['mycode']  ):
            print(f"i... {config.CONFIG['mycode']} ... trusted")
            args[0] = config.CONFIG['mycode']
        else:
            print(f"X... {config.CONFIG['mycode']} NOT TRUSTED")



    if args[0].find("{config.CONFIG['mycode']}")>=0 or args[0]==config.CONFIG['mycode']:
        print(f"i... I am running {config.CONFIG['mycode']} !!! ")
    else:
        print(f"X... not running the code from .config yet ...")

    #print(f"/{args[0]}/")
    #print(f"/{config.CONFIG['mycode']}/")
    #print("find == ", args[0].find("{config.CONFIG['mycode']}") )

    print()
    #---first-terminate all WEB ------
    process.terminate()
    process.join(timeout=1.0)
    #time.sleep(1)
    #-------------- prepare for respawn
    while t>0:
        pubt = f'X... Re-spawning in {t:5} seconds;  {args}  #'
        print( pubt , end = "\r")
        time.sleep( 1 )
        print( " "*len(pubt) , end = "\r")
        t-=1


    args.insert(0, sys.executable) # this is /ur/bin/python
    #os.chdir(_startup_cwd)
    os.execv(sys.executable, args)





def API(  ):

    print('D... In API n',config.CONFIG['port'])
    web.app.run(host='0.0.0.0', port=config.CONFIG['port'], threaded=True)








#=================================================================================================================
def main(cmd = "help", debug=False):
    ''' Main function of the project
    '''





    if not debug:
        _print = print # keep a local copy of the original print
        builtins.print =lambda *args, **kwargs:  None  if ("args" in locals() )and( len(args)>1 )and (isinstance(args[0], str)) and (args[0].find("D...")==0) else  _print( *args, **kwargs) if ('file' in kwargs) else _print( "{}".format(Bcolors.FAIL   if (("args" in locals() )and( len(args)>0 )and(isinstance(args[0], str)) and (args[0].find("X...")>=0)) else Bcolors.ENDC) , *args, Bcolors.ENDC, **kwargs, file=sys.stderr)
    else:
        # debug - show all + colors
        _print = print # keep a local copy of the original print
        builtins.print =lambda *args, **kwargs:   _print( *args, **kwargs) if ('file' in kwargs) else _print( "{}".format(Bcolors.FAIL   if ( ("args" in locals() )and(len(args)>0)and(isinstance(args[0], str)) and (args[0].find("X...")>=0)) else Bcolors.OKGREEN if  (("args" in locals() )and( len(args)>0 )and(isinstance(args[0], str)) and (args[0].find("i...")>=0)) else Bcolors.ENDC  ), *args, Bcolors.ENDC, **kwargs, file=sys.stderr)

    #=============version
    print("i...   version:",__version__,"___________________________________________________----")



    if cmd.find("help")==0:
        print('''
INSTALL: apt install python3-gpg

 ver      ... verify the code in .config with gpg
reinstall ... install  new version of the code( git,bump,gpg...)
run       ...  just start
run -d    ...  debug colors

------------------remote
curl http://localhost:8999/cmd/reload
curl http://localhost:8999/cmd/addpeer
curl http://localhost:8999/cmd/delpeer
curl http://localhost:8999/cmd/delconf ... delete config totally
curl http://localhost:8999/cmd/quit
curl http://localhost:8999/cmd/expression/value
curl -i -X PUT -F name=Test -F filedata=@mycode.py "http://localhost:8999/uploader"
''')
        sys.exit(0)


    if cmd.find("ver")==0:
        gpg_verify_mycode()
        sys.exit(0)



    if cmd=="reinstall":
        print(f"i... {__version__}")
        batch = f"""git commit -a -m stickygpg_automatic_sending
bumpversion patch
stickytape bin_selfan.py --copy-shebang  > mycode.py
rm mycode.py.asc
gpg --armor  --detach-sign  mycode.py
curl -i -X PUT -F name=Test -F filedata=@mycode.py.asc "http://localhost:8999/uploader"
curl -i -X PUT -F name=Test -F filedata=@mycode.py "http://localhost:8999/uploader"
curl http://localhost:8999/cmd/reload"""

        print(f"D... runnning batch\n{batch}")
        print()
        print()
        time.sleep(1)
        for i in batch.split("\n"):
            print("i... ",i)
            print("               cancel with Ctrl-c .... ")
            time.sleep(0.1)
            result = sp.run( i , shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
        print()
        sys.exit(0)






    #print("i... testing info  message",1,2)
    #print("D... testing debug message",3,4)
    #print("X... testing alert message",5,6)
    #print(7,8)

    if cmd.find("run")==0:

        myip = get_myip("tailscale0")
        if myip is None:
            print("X... NOT A MEMBER OF THE SOCIETY ... just respawn")
        else:
            config.CONFIG['myip'] = myip
            config.save_config()
        #print("D... sleeping before starting API")
        #time.sleep(3)

        mmcreate() # to be sure that good length is there

        p = multiprocessing.Process(target=API)
        p.start()

        t = 3600
        print(f"i... PROCESSING FOR {t} SECONDS")
        if process_commands( t=t ):
            p.terminate()
            p.join(timeout=1.0)
            print("X... normal termination on command")
            sys.exit(0)

        # if result not True -  just respawn
        respawn( t= 2, process = p) # warns for t seconds and kills process + respawns





if __name__=="__main__":
    Fire(main)
